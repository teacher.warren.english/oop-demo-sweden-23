﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoSweden23
{
    internal enum WoodTypes
    {
        Mahogany,
        Rosewood,
        Pine,
        GrandOak,
    }
}
