﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoSweden23
{
    internal abstract class Guitar
    {
        public string Name { get; set; }

        public Guitar(string name)
        {
            Name = name;
        }

        public virtual void DisplayGuitarName()
        {
            Console.WriteLine($"My name is {Name} (generic)");
        }

        public abstract void MakeNoise();
    }
}
