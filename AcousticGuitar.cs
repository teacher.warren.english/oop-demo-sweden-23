﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoSweden23
{
    internal class AcousticGuitar : Guitar
    {
        public WoodTypes WoodType { get; set; }
        public AcousticGuitar(string name, WoodTypes wood) : base(name)
        {
            WoodType = wood;
        }

        public override void MakeNoise()
        {
            Console.WriteLine($"{Name} is making some blissful sounds 😊");
        }

        //public WoodTypes DisplayWoodType()
        //{
        //    return WoodType;
        //}
    }
}
