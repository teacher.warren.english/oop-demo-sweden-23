﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoSweden23
{
    internal class ElectricGuitar : Guitar, ISmashable
    {
        public string Pickups { get; set; }
        public int Id { get; set; }
        public bool IsCool { get; set; }

        public ElectricGuitar(string name, string pickups) : base(name)
        {
            Pickups = pickups;
        }

        public override void DisplayGuitarName()
        {
            Console.WriteLine($"I'm an electric guitar, and my name is {Name}");
        }

        public override void MakeNoise()
        {
            Console.WriteLine($"{Name} is making noises ⚡⚡⚡");
        }

        public void SmashGuitar()
        {
            Console.WriteLine($"Smashing the {Name}. It goes crunch :(");
        }
    }

    public enum ElectricGuitarSomething
    {
        A,
        B,
        C,
    }
}
