# OOP Demo

A demonstration of basic object oriented programming concepts in C#, for Sweden group August 2023.
Uses the example of a base "Guitar" class with the child classes "ElectricGuitar", and "AcousticGuitar", and introduces using interfaces with "ISmashable".

©️ Noroff Accelerate

## Installation

Clone the repository by running the command:
```bash
git clone https://gitlab.com/teacher.warren.english/oop-demo-sweden-23.git
```

## Contributing

@teacher.warren.english

## License

[MIT](https://choosealicense.com/licenses/mit/)
