﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoSweden23.Nested_Folder
{
    internal class Banjo
    {
        public string Name { get; set; }

        public Banjo(string name)
        {
            Name = name;
        }
    }
}
