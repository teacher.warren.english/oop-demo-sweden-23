﻿using OOPDemoSweden23;
using OOPDemoSweden23.Nested_Folder;

AcousticGuitar myAcoustic = new AcousticGuitar("Ibanez", WoodTypes.GrandOak);
ElectricGuitar myElectric = new ElectricGuitar("Bzzzz", "Humbuckers");

Banjo myBanjo = new("Mumford & Son's Fav Banjo");

myElectric.DisplayGuitarName();
myAcoustic.DisplayGuitarName(); // alt + arrow

List<Guitar> myGuitars = new List<Guitar>() { myAcoustic, myElectric };

//myGuitars.Add(myElectric);
//myGuitars.Add(myAcoustic);

foreach (var g in myGuitars)
{
    g.MakeNoise();
}

Console.WriteLine($"The acoustic guitar is made of {myAcoustic.WoodType}");

myElectric.SmashGuitar();

ISmashable myBlueElectricGuitar = new ElectricGuitar("Blue Guitar", "EMGs");

myBlueElectricGuitar.SmashGuitar();